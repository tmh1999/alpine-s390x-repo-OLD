[ TEMPORARY ]

This repository contains packages serving Alpine s390x (https://hub.docker.com/r/tmh1999/alpine-s390x)


Add this repo to /etc/apk/repositories in your Alpine s390x Docker image by running :
```
$ sudo echo https://gitlab.com/tmh1999/alpine-s390x-repo/raw/master/alpine/edge/main > /etc/apk/repositories

$ sudo echo https://gitlab.com/tmh1999/alpine-s390x-repo/raw/master/alpine/edge/community >> /etc/apk/repositories

# testing packages only
$ sudo echo https://gitlab.com/tmh1999/alpine-s390x-repo/raw/master/alpine/edge/testing >> /etc/apk/repositories
```

